<?php

namespace App\Controllers;

use DB\DB;

abstract class Controller {

	private $db;
	private $table;

	public function connect( $table ) {
		$this->table = $table;
		$this->db = ( new DB() )->connect();
		return $this->db;
	}

	public function get() {
		if ( $this->db ) {
			$list = [];
			$select = $this->db->query( "SELECT * FROM {$this->table}" );
			while ( $row = $select->fetchObject() ) {
				$list[] = $row;
			}
			return $list;
		}
	}

	public function insert( $nome ) {
		if ( $this->db ) {
			$id = uniqid();
			$this->db->query( "INSERT INTO {$this->table} (id,nome) VALUES ('{$id}', '{$nome}')" );
		}
	}

	public function delete( $id ) {
		if ( $this->db ) {
			$select = $this->db->prepare( "DELETE FROM {$this->table} WHERE id = :id" );
			$select->bindValue( ':id', $id );
			$select->execute();
		}
	}

	public function update( $id, $nome ) {
		if ( $this->db ) {
			$select = $this->db->prepare( "UPDATE {$this->table} SET nome = :nome WHERE id = :id" );
			$select->bindValue( ':nome', $nome );
			$select->bindValue( ':id', $id );
			$select->execute();
		}
	}

	public function getById( $id ) {
		if ( $this->db ) {
			$select = $this->db->prepare( "SELECT nome FROM {$this->table} WHERE id = :id" );
			$select->bindParam( ':id', $id );
			$select->execute();
			return $select->fetchColumn();
		}
	}

}