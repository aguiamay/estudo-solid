<?php

namespace DB;

class DB {

	private $pdo;

	public function connect() {
		if ( null === $this->pdo ) {
			try {
				$this->pdo = new \PDO( 'sqlite:db/solidb');
			} catch ( \PDOException $e ) {
				return false;
			}
		}
		return $this->pdo;
	}

}